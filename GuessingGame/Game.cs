﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Game : IGame
    {
        public string DifficultyLevel { get; set; }

        private string CurrentWord;

        public int NumberOfGuesses { get; private set; }

        private bool[] LettersGuessed;
        // This array represents the correct guessings of the player, according to the word's letters index.

        private bool playerWon;

        public Game(string difficultyLevel)
        {
            if (difficultyLevel != "Easy" && difficultyLevel != "Hard")
                throw new InvalidDiffcultyLevelException("Diffuclty level must be 'Easy' or 'Hard'");

            this.DifficultyLevel = difficultyLevel;

            InitializeGame();
        }

        public void GameForm()
        {
            ClearScreen();

            while (playerWon == false)
            {
                Console.WriteLine("Please try to guess the next hidden word: \n");

                ShowHiddenWord();

                Console.WriteLine();

                char guess;
                try
                {
                    guess = Convert.ToChar(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("* Only a letter can be typed *");
                    Console.ReadLine();
                    continue;
                }

                if (CheckGuess(guess))
                    Console.WriteLine("Correct!");

                NumberOfGuesses++;

                if (!LettersGuessed.Contains(false)) // In case all letters was guessed currectly
                {
                    playerWon = true;
                }
                else
                    Console.WriteLine("Wrong!");

                System.Threading.Thread.Sleep(700);

                ClearScreen();
            }

            Console.WriteLine("Cogratulations! You succeeded! The secret word is: "+CurrentWord);

            Console.WriteLine($"\n You used {NumberOfGuesses} guesses in this game.");

            Console.WriteLine("Would you like to play again? y/n");

            if (Console.ReadLine() == "y")
                InitializeGame();
        }

        public void InitializeGame() // Resets the game
        {
            CurrentWord = GetWord();
            NumberOfGuesses = 0;
            LettersGuessed = new bool[CurrentWord.Count()];

            for (int i = 0; i < LettersGuessed.Length; i++)
            {
                LettersGuessed[i] = false;
            }

            playerWon = false;

            GameForm();
        }

        public bool CheckGuess(char guess)
        {
            if ( String.IsNullOrEmpty( Convert.ToString(guess) ) )
            {
                Console.WriteLine("Not a valid guess!");
                return false;
            }
                
            if ( CurrentWord.Contains( Convert.ToString(guess) ) )
            {
                for (int i = 0; i < CurrentWord.Count(); i++)
                {
                    if (CurrentWord.ElementAt(i) == guess)
                        LettersGuessed[i] = true;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ShowHiddenWord()
        {
            for (int i = 0; i < CurrentWord.Count(); i++)
            {
                if (LettersGuessed[i])
                    Console.Write(CurrentWord.ElementAt(i)+" ");
                else
                    Console.Write("_ ");
            }
        }

        public string GetWord()
        {
            if (DifficultyLevel == "Easy")
                return Words.GetRandomEasyWord();
            else if (DifficultyLevel == "Hard")
                return Words.GetRandomHardWord();
            else
                return null;
        }

        public void ClearScreen()
        {
            Console.Clear();
        }
    }
}
