﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    interface IGame
    {
        void GameForm();
        void ClearScreen();
        string GetWord();
        void ShowHiddenWord();
        bool CheckGuess(char guess);
        void InitializeGame();
    }
}
