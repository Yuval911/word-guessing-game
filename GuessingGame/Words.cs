﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    static class Words
    {
        public static List<string> WordsListEasy { get; set; }
        public static List<string> WordsListHard { get; set; }

        static Words()
        {
            WordsListEasy = new List<string>
            {
                "cat", "dog", "table", "house", "school", "sky", "earth",
                "man", "woman", "child", "toy", "tree", "plant"
            };

            WordsListHard = new List<string>
            {
                "astronout", "california", "overloading", "serialization",
                "debugging", "component", "notification", "bookmarks"
            };
        }

        public static string GetRandomEasyWord()
        {
            Random random = new Random();

            int index = random.Next(0, WordsListEasy.Count() );

            return WordsListEasy[index];
        }

        public static string GetRandomHardWord()
        {
            Random random = new Random();

            int index = random.Next(0, WordsListHard.Count());

            return WordsListHard[index];
        }
    }
}
