﻿using System;
using System.Runtime.Serialization;

namespace GuessingGame
{
    [Serializable]
    internal class InvalidDiffcultyLevelException : Exception
    {
        public InvalidDiffcultyLevelException()
        {
        }

        public InvalidDiffcultyLevelException(string message) : base(message)
        {
            
        }

        public InvalidDiffcultyLevelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidDiffcultyLevelException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}